import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      alias: '/login',
      component: () => import('./modules/Login.vue'),
      name: 'login',
    },
    {
      path: '/register-account',
      component: () => import('./modules/RegisterAccount.vue'),
      name: 'register-account',
    },
    {
      path: '/student-list',
      component: () => import('./modules/student/StudentPage.vue'),
      name: 'student-list',
    },
  ],
})
