import { getAllStudents } from '../service/index'

const state = {
    studentList: [],
}

const mutations = {
  SET_STUDENT_LIST (state, studentList) {
    state.studentList = studentList
  },
}

const actions = {
  async getAllStudents (context) {
    const response = await getAllStudents()
    console.log('response action...', response)
    context.commit('SET_STUDENT_LIST', response.data)
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
