import axios from '../../../plugins/axios'

const BASE_URL = process.env.VUE_APP_BASE_URL
export async function getAllStudents () {
  try {
    const respone = await axios.get(`${BASE_URL}/student/getAllStudents`)
    return respone.data
  } catch (error) {
    console.log(error)
  }
}
