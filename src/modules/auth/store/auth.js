import { login, register } from '../service/index'

const state = {
}

const mutations = {
}

const actions = {
  async login (context, request) {
    const response = await login(request)
    return response.data
  },
  async register (context, request) {
    const response = await register(request)
    return response
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
