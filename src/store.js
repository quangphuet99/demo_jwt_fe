import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth/store/auth'
import student from './modules/student/store/student'
import user from './modules/user/store/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    student,
    user,
  },
  state: {
  },
  mutations: {
  },
  actions: {
  },
})
