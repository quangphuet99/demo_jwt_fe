module.exports = {
    devServer: {
      port: 4200,
      disableHostCheck: true,
    },
  
    transpileDependencies: ['vuetify'],
  
    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: false,
      },
    },
  }
  
